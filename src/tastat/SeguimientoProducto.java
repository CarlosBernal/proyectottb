package tastat;


import javafx.event.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicLong;

import com.sun.javafx.binding.SelectBinding.AsDouble;

import Entidades.Client;
import Entidades.Comanda;
import Entidades.Empresa;
import Entidades.LotDesglossat;
import Entidades.Producte;
import Entidades.Proveidor;
import EntidadesParaVisual.LotVisual;
import EntidadesParaVisual.ProducteVisual;
import javafx.application.Application;
import javafx.beans.binding.DoubleBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableView.ResizeFeatures;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import tastat.TableViewEditingWithBinding.TextFieldCellFactory;

@SuppressWarnings("restriction")
public class SeguimientoProducto extends Application implements Initializable{	
	
	//DEFINIR VALORES TABLA 
	
	@FXML
	private TableView<ProducteVisual> tablaProducto = new TableView<ProducteVisual>();
	@FXML
	
	private TableView<LotVisual> tablaLots = new TableView<LotVisual>();
	@FXML
	private Button nuevoProducto;
	@FXML
	private Button modificarProducto;
	@FXML
	private TextField filtroID;
	@FXML
	private Button eliminar;
	@FXML
	private TextField filtroID2;
	@FXML
	private TextField filtroNom;
	
	public static int retornoMaestro;
	
	ObservableList<ProducteVisual> datos = null;
	
	ObservableList<LotVisual> datoslots = null;
	
	
	public SeguimientoProducto() {
		
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
	
	public void iniciartablas() {
		
	}
	
	public void avisoCambios() {
		ProducteVisual prodVisual;
		ArrayList<ProducteVisual> listaProdVisual = new ArrayList<>();
		
		for (Producte p : Main.getElMeuMagatzem().getMagatzem()) {


			String quantitat = (p.getStock()+"").toLowerCase();
			String codi = (p.getCodiProducte()+"").toLowerCase();	
			String nom = (p.getNomProducte()+"").toLowerCase();
			String tipus = (p.getTipus().toString()+"").toLowerCase();
			String unitat = (p.getUnitatMesura()+"").toLowerCase();
			String desc;
			if (p.getDescripcio()==null) {
				desc = "";
			}else {
				desc = p.getDescripcio().toString()+"";
			}
			

			prodVisual = new ProducteVisual(codi, nom,quantitat,tipus,unitat,desc);
			listaProdVisual.add(prodVisual);
 		  }  
		
		
		datos = FXCollections.observableArrayList(listaProdVisual);				
		tablaProducto.setItems(datos);
		
		
	}
	
	
	
	@FXML
	public void lanzarmodificarProducto(ActionEvent event){ 
		
		if (tablaProducto.getSelectionModel().getSelectedItem()==null) {
			gestionarAvisoModificar();
			
		}else {
			
		Stage parentStage = (Stage)((Button) event.getSource()).getScene().getWindow(); 
		MaestroProducte.setProducte(null);
		String id = tablaProducto.getSelectionModel().getSelectedItem().getCodiProducte();
		
		for(Producte p: Main.getElMeuMagatzem().getMagatzem()) {
			if((p.getCodiProducte()+"").equals(id)) {
				MaestroProducte.setProducte(p);
			}
		}
		
		Stage primaryStage = new Stage();
		Parent root=null;
		try {
			root = FXMLLoader.load(getClass().getClassLoader().getResource("tastat/MaestroProducto.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root);
		primaryStage.setTitle("Catalogo de Productes");
		primaryStage.setScene(scene);
		primaryStage.initModality(Modality.APPLICATION_MODAL);
		primaryStage.initOwner(parentStage);
		primaryStage.show();
		}
	}
	
	public void gestionarAvisoModificar() {
		Alert aviso = new Alert(AlertType.WARNING);
		aviso.setTitle("Modificaci�");
		aviso.setHeaderText(null);
		aviso.setContentText("Si us plau, seleccioni un producte a modificar");
		aviso.showAndWait();
	}
	
	@FXML
	public void lanzarnuevoProducto(ActionEvent event){ 
		Stage parentStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
		Stage primaryStage = new Stage();
		MaestroProducte.setProducte(null);
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("tastat/MaestroProducto.fxml"));
		Parent root = null;
		try {
			root = loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("*************************************************************");
		}

		MaestroProducte controller = (MaestroProducte) loader.getController();
//	        controller.setAction(action);
		controller.setParentController(this);
//	        controller.setObject(object); 

//		
//		try {
//			root = FXMLLoader.load(getClass().getClassLoader().getResource("tastat/MaestroProducto.fxml"));
//			//pasarle un parametro
//			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		Scene scene = new Scene(root);
		primaryStage.setTitle("A�adir producto nuevo");
		primaryStage.setScene(scene);
		primaryStage.initModality(Modality.APPLICATION_MODAL);
		primaryStage.initOwner(parentStage);

		// primaryStage.show();
		primaryStage.showAndWait();

	}
	
	@FXML
	public void eliminarProducte() {
		if (tablaProducto.getSelectionModel().getSelectedItem() != null) {
			String id = tablaProducto.getSelectionModel().getSelectedItem().getCodiProducte();
			int index = 999;
			for (int i = 0; i < Main.getElMeuMagatzem().getMagatzem().size(); i++) {
				if ((Main.getElMeuMagatzem().getMagatzem().get(i) != null
						&& (Main.getElMeuMagatzem().getMagatzem().get(i).getCodiProducte() + "").equals(id + ""))) {
					index = i;
				}
			}
			Main.getElMeuMagatzem().getMagatzem().remove(index);

			refrescar();
		}
		else {
			gestionarAvisoEliminar();
		}

	}

	private void gestionarAvisoEliminar() {
		Alert aviso = new Alert(AlertType.WARNING);
		aviso.setTitle("Eliminaci�");
		aviso.setHeaderText(null);
		aviso.setContentText("Si us plau, seleccioni un producte a eliminar");
		aviso.showAndWait();
		
	}

	public void refrescar() {
		tablaProducto.getColumns().clear();
		initialize(null, null);
	}
	

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
		/*TABLAS PRODUCTO*/
		
		TableColumn <ProducteVisual, String> colID = new TableColumn<ProducteVisual, String>("ID"); 
		colID.setCellValueFactory(new PropertyValueFactory<ProducteVisual,String>("codiProducte"));
		
		TableColumn <ProducteVisual, String> colTipoProd= new TableColumn<ProducteVisual, String>("Tipo Producto"); 
		colTipoProd.setCellValueFactory(new PropertyValueFactory<ProducteVisual,String>("tipoProducte"));
		
		
		TableColumn <ProducteVisual, String> colNomProducte = new TableColumn<ProducteVisual, String>("Nom Producte");
		colNomProducte.setCellValueFactory(new PropertyValueFactory<ProducteVisual,String>("nomProducte"));
		
		
		TableColumn <ProducteVisual, String> colQuantitat = new TableColumn<ProducteVisual, String>("Quantitat");
		colQuantitat.setCellValueFactory(new PropertyValueFactory<ProducteVisual,String>("quantitat"));		
		
		TableColumn <ProducteVisual, String> colDesc = new TableColumn<ProducteVisual, String>("Descripci�"); 
		colDesc.setCellValueFactory(new PropertyValueFactory<ProducteVisual,String>("descripcio"));
		
		
		TableColumn <ProducteVisual, String> colUnitat = new TableColumn<ProducteVisual, String>("Unitat de Mesura"); 
		colUnitat.setCellValueFactory(new PropertyValueFactory<ProducteVisual,String>("unitat"));
		//colUnitat.setCellFactory(new TextFieldCellFactory());
		
		/*TABLAS LOTES DE PRODUCTO*/
		
//		TableColumn <LotVisual, String> colidLot = new TableColumn<LotVisual, String>("idLot"); 
//		colidLot.setCellValueFactory(new PropertyValueFactory<LotVisual,String>("idLot"));
//		
//		TableColumn <LotVisual, String> colDataEntrada= new TableColumn<LotVisual, String>("Data Entrada"); 
//		colDataEntrada.setCellValueFactory(new PropertyValueFactory<LotVisual,String>("dataEntrada"));
//		
//		
//		TableColumn <LotVisual, String> colDataCaducitat = new TableColumn<LotVisual, String>("Data Caducitat");
//		colDataCaducitat.setCellValueFactory(new PropertyValueFactory<LotVisual,String>("dataCaducitat"));
//		
//		
//		TableColumn <LotVisual, String> colQuantitatLot = new TableColumn<LotVisual, String>("Quantitat");
//		colQuantitatLot.setCellValueFactory(new PropertyValueFactory<LotVisual,String>("quantitat"));		
//		
		LotVisual lotVisual;
		ProducteVisual prodVisual;
		ArrayList<ProducteVisual> listaProdVisual = new ArrayList<>();
		ArrayList<LotVisual> listaLotVisual = new ArrayList<>();
		
		
		//Empresa mgz = new Empresa(new ArrayList<Producte>(), new ArrayList<Client>(), new ArrayList<Comanda>(), new ArrayList<Proveidor>());
		
		//Main.generarDadesBasiques(mgz);
		
		//System.out.println(mgz);
		
		
		for (Producte p: Main.getElMeuMagatzem().getMagatzem()) {
			
			
			String quantitat = (p.getStock()+"").toLowerCase();
			String codi = (p.getCodiProducte()+"").toLowerCase();	
			String nom = (p.getNomProducte()+"").toLowerCase();
			String tipus = (p.getTipus().toString()+"").toLowerCase();
			String unitat = (p.getUnitatMesura()+"").toLowerCase();
			String desc;
			if (p.getDescripcio()==null) {
				desc = "";
			}else {
				desc = p.getDescripcio().toString()+"";
			}
			
			
			
			prodVisual = new ProducteVisual(codi, nom,quantitat,tipus,unitat,desc);
			listaProdVisual.add(prodVisual);
			
//			for (LotDesglossat lt: p.getLots()) {
//				String idlot = lt.getLot()+"";
//				String gataEntr = lt.getDataEntrada()+"";
//				String dataCad = lt.getDataCaducitat()+"";
//				String quant = lt.getQuantitat()+"";
//				
//				lotVisual = new LotVisual(idlot,gataEntr,dataCad,quant);
//				listaLotVisual.add(lotVisual);
//		}
				
			
		}
		
		/*omplir taula de lots*/
//		datoslots = FXCollections.observableArrayList(listaLotVisual);		
//		tablaLots.setItems(datoslots);
		
		/*Omplir taula de productes*/
		datos = FXCollections.observableArrayList(listaProdVisual);				
		tablaProducto.setItems(datos);
		
//		System.out.println(datos.get(0).getCodiProducte());
//		System.out.println(datos.get(0).getnomProducte());
//		System.out.println(datos.get(0).getQuantitat());
		
		
		//colNomProducte.setResizable(true);
		//tablaproducto.setBackground(Background.EMPTY);
//		tablaproducto.getColumns().add(0, colNomProducte);
//		String prueba = tablaproducto.getColumns().get(0).getText().toString();
//		
//		System.out.println(prueba);
		
		tablaProducto.getColumns().addAll(colNomProducte,colID,colQuantitat,colTipoProd,colUnitat,colDesc);
		//tablaLots.getColumns().addAll(colidLot,colDataEntrada,colDataCaducitat,colQuantitatLot);
		
		
		
		
       
		 
		
		FilteredList<ProducteVisual> filteredData = new FilteredList<>(datos, p -> true);
		
		 // 2. Set the filter Predicate whenever the filter changes.
		filtroID.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(person -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (person.getCodiProducte().toLowerCase().contains(lowerCaseFilter)) { 
                    return true; // Filter matches first name.
                } else if (person.getNomProducte().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                } else if (person.getQuantitat().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                } else if (person.getTipoProducte().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                } else if (person.getUnitat().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }else if (person.getDescripcio().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                return false; // Does not match.--
            });
        });
		
		
		
		
		// 3. Wrap the FilteredList in a SortedList. 
        SortedList<ProducteVisual> sortedData = new SortedList<>(filteredData);
        
        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tablaProducto.comparatorProperty());
        
        // 5. Add sorted (and filtered) data to the table.
        tablaProducto.setItems(sortedData);
        
        
        //******************
        FilteredList<ProducteVisual> filteredData2;
        
        FilteredList<ProducteVisual> fl = new FilteredList<ProducteVisual>(sortedData);
        
        filteredData2 = filtrarNom(fl);
        SortedList<ProducteVisual> sortedData2 = new SortedList<>(filteredData2);
        sortedData2.comparatorProperty().bind(tablaProducto.comparatorProperty());
        tablaProducto.setItems(sortedData2);
        
      //******************
        FilteredList<ProducteVisual> filteredData3;
        
        FilteredList<ProducteVisual> fl1 = new FilteredList<ProducteVisual>(sortedData2);
        
        filteredData3 = filtrarID(fl1);
        SortedList<ProducteVisual> sortedData3 = new SortedList<>(filteredData3);
        sortedData3.comparatorProperty().bind(tablaProducto.comparatorProperty());
        tablaProducto.setItems(sortedData3);
      
    

//		String prueba  = (String) tablaproducto.getColumns().get(0).getCellData(0);
	
		//String prueba = tablaproducto.getItems().get(0).getnomProducte();
		
		//System.out.println(prueba);
        
        
        //GUIUtils.autoFitTable(tablaproducto);
        
       // tablaproducto.autosize();
       //tablaproducto.columnResizePolicyProperty();
        
        //tablaproducto.resizeColumn(colNomProducte, 200);
       // tablaproducto.isResizable();
       
        
	}
	
	@FXML	
	public FilteredList<ProducteVisual> filtrarID(FilteredList<ProducteVisual> filteredData) {
		 // 2. Set the filter Predicate whenever the filter changes.
		filtroID2.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(person -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (person.getCodiProducte().toLowerCase().contains(lowerCaseFilter)) { 
                    return true; // Filter matches first name.
                }
                return false; // Does not match.--
            });
        });
		
		return filteredData;
		
	}
	
	@FXML	
	public FilteredList<ProducteVisual> filtrarNom(FilteredList<ProducteVisual> filteredData) {
		 // 2. Set the filter Predicate whenever the filter changes.
		filtroNom.textProperty().addListener((observable, oldValue, newValue) -> {
           filteredData.setPredicate(person -> {
               // If filter text is empty, display all persons.
               if (newValue == null || newValue.isEmpty()) {
                   return true;
               }
               
               // Compare first name and last name of every person with filter text.
               String lowerCaseFilter = newValue.toLowerCase();
               
               if (person.getNomProducte().toLowerCase().contains(lowerCaseFilter)) { 
                   return true; // Filter matches first name.
               }
               return false; // Does not match.--
           });
       });
		
		return filteredData;
		
	}
	
	
	

	@Override
	public void start(Stage primaryStage) throws Exception { 	  
	    	  
	      
		
	}
}
