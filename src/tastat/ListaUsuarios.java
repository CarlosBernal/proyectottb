package tastat;

import java.util.ArrayList;

import Entidades.Usuari;

public class ListaUsuarios {

	ArrayList<Usuari> listaUsuari = new ArrayList<>();

	public Usuari canviUsuari(String nick) {

		for (Usuari u : listaUsuari) {
			if (u.getNom().equals(nick)) {

				return u;
			} else {
				System.out.println("usuario no encontrado");
				return null;
			}
		}

		return null;
	}

	public void altaUsuari(Usuari nou) {
		if (!nou.getNom().equalsIgnoreCase("") && nou != null) {
			for (Usuari n : listaUsuari) {
				if (n.getNom().equals(nou.getNom())) {
					listaUsuari.add(nou);// has creat un nou usuari
				}
			}
		}

	}

	public void baixaUsuari(Usuari baixa) {
		if (!baixa.getNom().equalsIgnoreCase("") && baixa != null) {
			for (Usuari n : listaUsuari) {
				if (!n.getNom().equals(baixa.getNom())) {
					listaUsuari.remove(baixa);
				}
			}
		}
	}

	public void modificacions(Usuari us ,String nick, String pass) {
		for (Usuari n : listaUsuari) {
			if (us.getNom().equals(n.getNom())) {
				n.setNom(nick);
			}
			if (us.getPass().equals(n.getPass())) {
				if (!pass.isEmpty()) /*^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$)*/ {
					n.setPass(pass);
				}else {
					
					//error de contraseņa
				}
				
			}
		}

	}

	public void cercaUsuari(int idClient, String nomClient) {

	}

	public ArrayList<Usuari> getUsuari() {
		return listaUsuari;
	}

	public void setUsuari(ArrayList<Usuari> usuari) {
		this.listaUsuari = usuari;
	}

}
