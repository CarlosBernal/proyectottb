package tastat;

import java.net.URL;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import Entidades.Client;
import Entidades.Comanda;
import Entidades.ComandaEstat;
import Entidades.ComandaLinia;
import Entidades.Producte;
import EntidadesParaVisual.LiniaComandaVisual;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Toggle;

@SuppressWarnings("restriction")
public class MaestroComanda extends Application implements Initializable {

	static private Comanda comanda;

	@FXML
	private Label LBLIdComanda;
	@FXML
	private TextField TFIdComanda;
	@FXML
	private Label IdClient;
	@FXML
	private TextField TFIdClient;
	@FXML
	private Label LBLIdClient;
	@FXML
	private DatePicker DataComanda;
	@FXML
	private DatePicker DataLliurament;
	@FXML
	private ToggleGroup TGEstatComanda;
	@FXML
	private RadioButton RBPendent;
	@FXML
	private RadioButton RBPreparada;
	@FXML
	private RadioButton RBTransport;
	@FXML
	private RadioButton RBLliurada;
	@FXML
	private Label LBLPorts;
	@FXML
	private TextField TFPorts;
	@FXML
	private Label LBLPreuTotal;
	@FXML
	private Button BTAfegirLinia;
	@FXML
	private Button BTEliminarLinia;
	@FXML
	private Button BTGrabar;
	@FXML
	private Button BTCancelar;
	@FXML
	private TableView<LiniaComandaVisual> tablaLiniasComanda;

	ArrayList<LiniaComandaVisual> listaLiniaComandaVisual = new ArrayList<>();

	ObservableList<LiniaComandaVisual> datos = null;

	ObservableList<LiniaComandaVisual> datosLiniasComanda = null;

	private SeguimientoComanda seguimiento = null;

	@FXML
	public void onEnterCodigo(ActionEvent ae) {
		boolean duplicado = false;
		for (Comanda c : Main.getElMeuMagatzem().getComandes()) {
			if (TFIdComanda.getText().equals(c.getIdComanda() + "")) {
				duplicado = true;
			}
		}
		if (duplicado) {
			gestionarErrorIdComanda();
		} else {
			deshabilitarCampos(false);
			RBLliurada.setDisable(true);
			TFIdClient.setFocusTraversable(true);
		}
	}

	@FXML
	public void onEnterCliente(ActionEvent ae) {
		boolean trobat = false;
		for (Client c : Main.getElMeuMagatzem().getClients()) {
			if ((c.getIdClient() + "").equals(TFIdClient.getText())) {
				LBLIdClient.setText(c.getNomClient());
				trobat = true;
			}
		}
		if (!trobat) {
			gestionarErrorIdCliente();
		}
	}

	// @FXML
	// public void onChangeEstat(ActionEvent ae) {
	// System.out.println("patata");
	// }

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("tastat/MaestroComanda.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setTitle("Comandes");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// inicializaci�n com�n
		LiniaComandaVisual liniaComandaVisual;
		tablaLiniasComanda.setEditable(true);

		TFPorts.setText("0.0");
		TGEstatComanda = new ToggleGroup();
		RBPendent.setToggleGroup(TGEstatComanda);
		RBPreparada.setToggleGroup(TGEstatComanda);
		RBTransport.setToggleGroup(TGEstatComanda);
		RBLliurada.setToggleGroup(TGEstatComanda);
		TGEstatComanda.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			public void changed(ObservableValue<? extends Toggle> ov, Toggle oldValue, Toggle newValue) {
				if (oldValue == RBPendent && (newValue == RBPreparada || newValue == RBTransport)) {
					checkOutComanda();
					tablaLiniasComanda.setEditable(false);
					BTAfegirLinia.setDisable(true);
					BTEliminarLinia.setDisable(true);
				} else if ((oldValue == RBPreparada || oldValue == RBTransport) && newValue == RBPendent) {
					revertCheckOut(tablaLiniasComanda.getItems().size());
					tablaLiniasComanda.setEditable(true);
					BTAfegirLinia.setDisable(false);
					BTEliminarLinia.setDisable(false);
				}
			}
		});

		TFPorts.textProperty().addListener((observable, oldValue, newValue) -> {
			try {
				LBLPreuTotal.setText((Double.parseDouble(LBLPreuTotal.getText()) - Double.parseDouble(oldValue)
						+ Double.parseDouble(newValue)) + "");
			} catch (NumberFormatException e) {
				TFPorts.setText("0.0");
				LBLPreuTotal.setText((Double.parseDouble(LBLPreuTotal.getText()) - Double.parseDouble(oldValue)
						+ Double.parseDouble(newValue)) + "");
			}
		});

		TableColumn<LiniaComandaVisual, String> colIdProducte = new TableColumn<LiniaComandaVisual, String>(
				"Id Producte");
		colIdProducte.setCellValueFactory(new PropertyValueFactory<LiniaComandaVisual, String>("idProducte"));
		colIdProducte.setCellFactory(TextFieldTableCell.<LiniaComandaVisual>forTableColumn());
		colIdProducte.setOnEditCommit((CellEditEvent<LiniaComandaVisual, String> t) -> {
			((LiniaComandaVisual) t.getTableView().getItems().get(t.getTablePosition().getRow()))
					.setIdProducte((t.getNewValue()));
			buscarProducto(t.getTablePosition().getRow());
		});
		TableColumn<LiniaComandaVisual, String> colNomProducte = new TableColumn<LiniaComandaVisual, String>(
				"Nom Producte");
		colNomProducte.setCellValueFactory(new PropertyValueFactory<LiniaComandaVisual, String>("nomProducte"));

		TableColumn<LiniaComandaVisual, String> colQuantitat = new TableColumn<LiniaComandaVisual, String>("Quantitat");
		colQuantitat.setCellValueFactory(new PropertyValueFactory<LiniaComandaVisual, String>("quantitat"));
		colQuantitat.setCellFactory(TextFieldTableCell.<LiniaComandaVisual>forTableColumn());
		colQuantitat.setOnEditCommit((CellEditEvent<LiniaComandaVisual, String> t) -> {
			((LiniaComandaVisual) t.getTableView().getItems().get(t.getTablePosition().getRow()))
					.setQuantitat((t.getNewValue()));
			recalcularImporte(t.getTablePosition().getRow());
		});

		TableColumn<LiniaComandaVisual, String> colPreu = new TableColumn<LiniaComandaVisual, String>("Preu");
		colPreu.setCellValueFactory(new PropertyValueFactory<LiniaComandaVisual, String>("preu"));
		colPreu.setCellFactory(TextFieldTableCell.<LiniaComandaVisual>forTableColumn());
		colPreu.setOnEditCommit((CellEditEvent<LiniaComandaVisual, String> t) -> {
			((LiniaComandaVisual) t.getTableView().getItems().get(t.getTablePosition().getRow()))
					.setPreu((t.getNewValue()));
			recalcularImporte(t.getTablePosition().getRow());
		});

		TableColumn<LiniaComandaVisual, String> colImporte = new TableColumn<LiniaComandaVisual, String>("Import");
		colImporte.setCellValueFactory(new PropertyValueFactory<LiniaComandaVisual, String>("importe"));

		tablaLiniasComanda.getColumns().addAll(colIdProducte, colNomProducte, colQuantitat, colPreu, colImporte);

		datos = FXCollections.observableArrayList(listaLiniaComandaVisual);
		tablaLiniasComanda.setItems(datos);

		if (comanda != null) {
			// �s modificar comanda
			deshabilitarCampos(false);
			TFIdComanda.setText(comanda.getIdComanda() + "");
			TFIdClient.setText(comanda.getClient().getIdClient() + "");

			for (Client c : Main.getElMeuMagatzem().getClients()) {
				if ((c.getIdClient() + "").equals(comanda.getClient().getIdClient() + "")) {
					LBLIdClient.setText(comanda.getClient().getNomClient());
				}
			}

			DataComanda.setValue(comanda.getDataComandaLocal());
			DataLliurament.setValue(comanda.getDataLliuramentLocal());

			switch (comanda.getEstat()) {
			case PENDENT:
				RBPendent.setSelected(true);
				RBLliurada.setDisable(true);
				break;
			case PREPARADA:
				RBPreparada.setSelected(true);
				tablaLiniasComanda.setEditable(false);
				BTAfegirLinia.setDisable(true);
				BTEliminarLinia.setDisable(true);
				break;
			case TRANSPORT:
				RBTransport.setSelected(true);
				tablaLiniasComanda.setEditable(false);
				BTAfegirLinia.setDisable(true);
				BTEliminarLinia.setDisable(true);
				break;
			case LLIURADA:
				RBLliurada.setSelected(true);
				deshabilitarCampos(true);
				TFIdComanda.setDisable(true);
				tablaLiniasComanda.setEditable(false);
				BTAfegirLinia.setDisable(true);
				BTEliminarLinia.setDisable(true);
				break;
			default:
				break;
			}

			TFPorts.setText(comanda.getPortes() + "");

			Double acumuladorPreu = 0.0;
			for (int i = 0; i < comanda.getLinies().size(); i++) {
				ComandaLinia cl = comanda.getLinies().get(i);

				String idProducte = cl.getProducte().getCodiProducte() + "";
				String nomProducte = cl.getProducte().getNomProducte();
				String quantitat = cl.getQuantitat() + "";
				String preu = cl.getPreuVenda() + "";

				liniaComandaVisual = new LiniaComandaVisual(idProducte, nomProducte, quantitat, preu);
				acumuladorPreu += Double.parseDouble(liniaComandaVisual.getImporte());
				listaLiniaComandaVisual.add(liniaComandaVisual);
			}

			acumuladorPreu += Double.parseDouble(TFPorts.getText());
			LBLPreuTotal.setText(acumuladorPreu + "");

			datos = FXCollections.observableArrayList(listaLiniaComandaVisual);
			tablaLiniasComanda.setItems(datos);

		} else {
			// �s nova comanda
			comanda = new Comanda();
			deshabilitarCampos(true);
		}
	}

	private void buscarProducto(int row) {
		boolean trobat=false;
		for (Producte p : Main.getElMeuMagatzem().getMagatzem()) {
			if ((p.getCodiProducte() + "").equals(tablaLiniasComanda.getItems().get(row).getIdProducte())) {
				tablaLiniasComanda.getItems().get(row).setNomProducte(p.getNomProducte());
				comanda.getLinies().set(row,
						new ComandaLinia(p, Integer.parseInt(tablaLiniasComanda.getItems().get(row).getQuantitat()),
								Double.parseDouble(tablaLiniasComanda.getItems().get(row).getPreu())));
				trobat=true;
			}
		}
		if(trobat) {
			refrescarLiniesComanda();
		}
		else {
			gestionarErrorIdProducto();
		}
	}

	private void recalcularImporte(int row) {
		tablaLiniasComanda.getItems().get(row)
				.setImporte((Double.parseDouble(tablaLiniasComanda.getItems().get(row).getPreu())
						* Integer.parseInt(tablaLiniasComanda.getItems().get(row).getQuantitat())) + "");
		for (Producte p : Main.getElMeuMagatzem().getMagatzem()) {
			if ((p.getCodiProducte() + "").equals(tablaLiniasComanda.getItems().get(row).getIdProducte())) {
				comanda.getLinies().set(row,
						new ComandaLinia(p, Integer.parseInt(tablaLiniasComanda.getItems().get(row).getQuantitat()),
								Double.parseDouble(tablaLiniasComanda.getItems().get(row).getPreu())));
			}
		}
		recalcularPreuTotal();
		refrescarLiniesComanda();
	}

	private void refrescarLiniesComanda() {
		LiniaComandaVisual liniaComandaVisual;
		tablaLiniasComanda.getItems().clear();
		listaLiniaComandaVisual.clear();
		datosLiniasComanda = FXCollections.observableArrayList(listaLiniaComandaVisual);

		Double acumuladorPreu = 0.0;
		for (int i = 0; i < comanda.getLinies().size(); i++) {
			ComandaLinia cl = comanda.getLinies().get(i);

			String idProducte = cl.getProducte().getCodiProducte() + "";
			String nomProducte = cl.getProducte().getNomProducte();
			String quantitat = cl.getQuantitat() + "";
			String preu = cl.getPreuVenda() + "";

			acumuladorPreu += cl.getPreuVenda() * cl.getQuantitat();
			liniaComandaVisual = new LiniaComandaVisual(idProducte, nomProducte, quantitat, preu);

			listaLiniaComandaVisual.add(liniaComandaVisual);
		}

		acumuladorPreu += Double.parseDouble(TFPorts.getText());
		LBLPreuTotal.setText(acumuladorPreu + "");

		datos = FXCollections.observableArrayList(listaLiniaComandaVisual);
		tablaLiniasComanda.setItems(datos);
	}

	@FXML
	public void AfegirLiniaComanda() {
		Producte p = new Producte("Nou producte");
		comanda.getLinies().add(new ComandaLinia(p, 0, 0.0));
		refrescarLiniesComanda();
	}

	@FXML
	public void eliminarLiniaComanda() {
		if (tablaLiniasComanda.getSelectionModel().getSelectedItem() != null) {
			int index = tablaLiniasComanda.getSelectionModel().getSelectedIndex();
			comanda.getLinies().remove(index);

			refrescarLiniesComanda();
		} else {
			gestionarAvisoAlEliminar();
		}
	}

	public void gestionarAvisoAlEliminar() {
		Alert aviso = new Alert(AlertType.WARNING);
		aviso.setTitle("Eliminaci�");
		aviso.setHeaderText(null);
		aviso.setContentText("Si us plau, seleccioni un linia a eliminar");
		aviso.showAndWait();
	}

	private void recalcularPreuTotal() {
		Double acumuladorPreu = 0.0;

		for (LiniaComandaVisual l : tablaLiniasComanda.getItems()) {
			acumuladorPreu += Double.parseDouble(l.getPreu());
		}

		acumuladorPreu += Double.parseDouble(TFPorts.getText());
		LBLPreuTotal.setText(acumuladorPreu + "");
	}

	private void checkOutComanda() {
		for (int row = 0; row < tablaLiniasComanda.getItems().size(); row++) {
			LiniaComandaVisual lc = new LiniaComandaVisual();
			lc = tablaLiniasComanda.getItems().get(row);

			for (Producte p : Main.getElMeuMagatzem().getMagatzem()) {
				if ((p.getCodiProducte() + "").equals(lc.getIdProducte())) {
					if (p.getStock() >= Integer.parseInt(lc.getQuantitat())) {
						String s = p.getNomProducte() + " Stock: " + p.getStock() + " - " + lc.getQuantitat();

						p.setStock(p.getStock() - Integer.parseInt(lc.getQuantitat()));

						s += " = " + p.getStock();
						System.out.println(s);
					} else {
						System.out.println("No hay suficiente stock para la linia: " + row + p.getNomProducte()
								+ " Stock actual:  Stock: " + p.getStock() + " Stock necesario: " + lc.getQuantitat());
						revertCheckOut(row);

						gestionarErrorSinStock(p.getNomProducte(), lc.getQuantitat(), p.getStock() + "", row);
						return;
					}
				}
			}
		}
	}

	private void revertCheckOut(int row) {
		System.out.println("Revirtiendo comanda");
		for (int i = 0; i < row; i++) {
			LiniaComandaVisual lc = new LiniaComandaVisual();
			lc = tablaLiniasComanda.getItems().get(i);

			for (Producte p : Main.getElMeuMagatzem().getMagatzem()) {
				if ((p.getCodiProducte() + "").equals(lc.getIdProducte())) {
					p.setStock(p.getStock() + Integer.parseInt(lc.getQuantitat()));
					System.out.println(p.getNomProducte() + " Stock: " + p.getStock());
				}
			}
		}
	}

	public void gestionarErrorIdComanda() {
		Alert aviso = new Alert(AlertType.WARNING);
		aviso.setTitle("Codi duplicat");
		aviso.setHeaderText(null);
		aviso.setContentText("Aquest codi ja es fa servir per a un altre registre.");
		aviso.showAndWait();
	}

	public void gestionarErrorIdCliente() {
		Alert aviso = new Alert(AlertType.WARNING);
		aviso.setTitle("Error");
		aviso.setHeaderText(null);
		aviso.setContentText("Aquest client no existeix");
		aviso.showAndWait();
	}
	
	public void gestionarErrorIdProducto() {
		Alert aviso = new Alert(AlertType.WARNING);
		aviso.setTitle("Error");
		aviso.setHeaderText(null);
		aviso.setContentText("Aquest producte no existeix");
		aviso.showAndWait();
	}

	public void gestionarErrorSinStock(String nomProducte, String requiredStock, String currentStock, int nLinia) {
		Alert aviso = new Alert(AlertType.WARNING);
		aviso.setTitle("Stock insuficient");
		aviso.setHeaderText(null);
		aviso.setContentText("No hi ha suficient Stock per la linia " + nLinia + ":\t Producte: " + nomProducte
				+ "Stock actual: " + currentStock + " Stock requerit: " + requiredStock);
		aviso.showAndWait();
	}

	public void grabar() {
		boolean nuevoRegistro = true;
		if (comprobarDatos()) {
			establecerRegistroModificado();
			for (int i = 0; i < Main.getElMeuMagatzem().getComandes().size(); i++) {
				if ((Main.getElMeuMagatzem().getComandes().get(i) != null && comanda != null
						&& (Main.getElMeuMagatzem().getComandes().get(i).getIdComanda() + "")
								.equals(comanda.getIdComanda() + ""))) {
					Main.getElMeuMagatzem().getComandes().set(i, comanda);
					nuevoRegistro = false;
				}
			}
			if (nuevoRegistro) {
				Main.getElMeuMagatzem().getComandes().add(comanda);
			}
		}

		seguimiento.refrescar();
		TFIdComanda.getScene().getWindow().hide();
	}

	private void establecerRegistroModificado() {
		comanda.setIdComanda(Integer.parseInt(TFIdComanda.getText()));

		for (Client c : Main.getElMeuMagatzem().getClients()) {
			if ((c.getIdClient() + "").equals(TFIdClient.getText())) {
				comanda.setClient(c);
			}
		}

		Date date = Date.from(DataComanda.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
		comanda.setDataComanda(date);
		date = Date.from(DataLliurament.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
		comanda.setDataLliurament(date);

		if (RBPendent.isSelected()) {
			comanda.setEstat(ComandaEstat.PENDENT);
		} else if (RBPreparada.isSelected()) {
			comanda.setEstat(ComandaEstat.PREPARADA);
		} else if (RBTransport.isSelected()) {
			comanda.setEstat(ComandaEstat.TRANSPORT);
		} else if (RBLliurada.isSelected()) {
			comanda.setEstat(ComandaEstat.LLIURADA);
		}
	}

	public void limpiarCampos() {
		TFIdClient.setText("");
		LBLIdClient.setText("");
		DataComanda.setValue(null);
		DataLliurament.setValue(null);
		TFPorts.setText("0.0");
		tablaLiniasComanda.getItems().clear();
		LBLPreuTotal.setText("0.0");
	}

	public void deshabilitarCampos(boolean disable) {
		TFIdComanda.setDisable(!disable);

		BTAfegirLinia.setDisable(disable);
		BTEliminarLinia.setDisable(disable);
		TFIdClient.setDisable(disable);
		DataComanda.setDisable(disable);
		DataLliurament.setDisable(disable);
		TFPorts.setDisable(disable);
		RBLliurada.setDisable(disable);
		RBPendent.setDisable(disable);
		RBPreparada.setDisable(disable);
		RBTransport.setDisable(disable);
	}

	private boolean comprobarDatos() {
		boolean ok = true;

		if (TFIdClient.getText().equals("")) {
			ok = false;
		}
		if (DataComanda.getValue() == null) {
			ok = false;
		}
		System.out.println("grabado: " + ok);
		return ok;
	}

	public TextField getTFIdClient() {
		return TFIdClient;
	}

	public void setTFIdClient(TextField tFIdClient) {
		TFIdClient = tFIdClient;
	}

	public Label getLBLIdClient() {
		return LBLIdClient;
	}

	public void setLBLIdClient(Label lBLIdClient) {
		LBLIdClient = lBLIdClient;
	}

	public static Comanda getComanda() {
		return comanda;
	}

	public static void setComanda(Comanda comanda) {
		MaestroComanda.comanda = comanda;
	}

	public Label getIdClient() {
		return IdClient;
	}

	public void setIdClient(Label idClient) {
		IdClient = idClient;
	}

	public SeguimientoComanda getParentController() {
		return seguimiento;
	}

	public void setParentController(SeguimientoComanda seguimientoComanda) {
		this.seguimiento = seguimientoComanda;
	}

}