package tastat;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;

import Entidades.Client;
import Entidades.ComandaLinia;
import Entidades.LotDesglossat;
import Entidades.Producte;
import Entidades.Tipus;
import Entidades.UnitatMesura;
import EntidadesParaVisual.LiniaComandaVisual;
import EntidadesParaVisual.LotVisual;
import javafx.application.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.stage.Stage;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.Button;

@SuppressWarnings("restriction")
public class MaestroProducte extends Application implements Initializable {

	static private Producte producte;
	@FXML
	private TextField idproducte;
	@FXML
	private TextField nomProducte;
	@FXML
	private TextField descripcio;
	@FXML
	private TextField preuVenda;
	@FXML
	private TextField stock;
	@FXML
	private TextField stockMinim;
	@FXML
	private TextField idProveidor;
	// @FXML
	// private ToggleGroup e; --> para luego hacer los grupos de radio
	@FXML
	private Button afegir;
	@FXML
	private RadioButton rdlitre;
	@FXML
	private RadioButton rduinitat;
	@FXML
	private RadioButton rdgrams;
	@FXML
	private RadioButton rdingredient;
	@FXML
	private RadioButton rdvendible;
	@FXML
	private TableView<LotVisual> tablaLots = new TableView<LotVisual>();

	ArrayList<LotVisual> listaLotVisual = new ArrayList<>();

	ObservableList<LiniaComandaVisual> datos = null;

	ObservableList<LotVisual> datoslots = null;

	private boolean cambioPantalla;

	private SeguimientoProducto segprod = null;
	@FXML
	private Button esborrar;
	@FXML
	private Button afegirLotNou;
	@FXML
	private Button eliminarLot;

	@FXML
	public void esborrar() {
		idproducte.setText("");
		nomProducte.setText("");
		descripcio.setText("");
		preuVenda.setText("");
		stock.setText("");
		stockMinim.setText("");
		idProveidor.setText("");
		afegir.setText("");
		rdlitre.setText("");
		rduinitat.setText("");

	}

	@FXML
	public void onEnter(ActionEvent ae) {
		deshabilitarCampos(false);
		nomProducte.setFocusTraversable(true);

	}

	@FXML
	public boolean comprobarTodosCampos() {

		if (idproducte.getText().isEmpty() || nomProducte.getText().isEmpty() || descripcio.getText().isEmpty()
				|| preuVenda.getText().isEmpty() || stock.getText().isEmpty() || stockMinim.getText().isEmpty()
				|| idProveidor.getText().isEmpty()) {

			Alert aviso = new Alert(AlertType.WARNING);
			aviso.setTitle("Validació de dades");
			aviso.setHeaderText(null);
			aviso.setContentText("Si us plau ompli tots els camps");
			aviso.showAndWait();
			return false;
		} else {
			comprovarInts();
			llenarDatos();
			cambioPantalla = true;
			if (cambioPantalla) {
				// close this window...
				segprod.avisoCambios();
				afegir.getScene().getWindow().hide();
			}
		}
		return true;
	}

	private void alertaInt() {
		Alert aviso = new Alert(AlertType.WARNING);
		aviso.setTitle("Validació de dades");
		aviso.setHeaderText(null);
		aviso.setContentText("Si us plau ompli els camps numérics correctament");
		aviso.showAndWait();

	}

	private void comprovarInts() {

		if (!idproducte.getText().matches("[0-9]+")) {
			alertaInt();
		} else if (!stock.getText().matches("[0-9]+")) {
			alertaInt();
		} else if (!stockMinim.getText().matches("[0-9]+")) {
			alertaInt();
		} else {

		}
	}

	public void grabar() {
		if (comprobarTodosCampos()) {
			Main.getElMeuMagatzem().getMagatzem().add(producte);
			SeguimientoProducto.retornoMaestro = 1;
		}
	}

	public void cancelar() {

	}

	public void deshabilitarCampos(boolean disable) {

		idproducte.setDisable(!disable);
		nomProducte.setDisable(disable);
		descripcio.setDisable(disable);
		preuVenda.setDisable(disable);
		stock.setDisable(disable);
		stockMinim.setDisable(disable);
		idProveidor.setDisable(disable);
		rdgrams.setDisable(disable);
		rdingredient.setDisable(disable);
		rdlitre.setDisable(disable);
		rduinitat.setDisable(disable);
		rdvendible.setDisable(disable);

	}

	private void llenarDatos() {
		Producte p;
		int stockToInt = Integer.parseInt(stockMinim.getText());
		p = new Producte(nomProducte.getText().toString(), UnitatMesura.GRAMS, stockToInt);
		Main.getElMeuMagatzem().getProductes().add(p);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("tastat/MaestroProducto.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setTitle("Producto");
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	@FXML
	public void afegirLotNou() {

		tablaLots.getColumns().clear();
		listaLotVisual.clear();
		producte.getLots().add(new LotDesglossat(0, new Date(), 0));
		datoslots = FXCollections.observableArrayList(listaLotVisual);
		initialize(null, null);

	}

	@FXML
	public void eliminarLot() {
		if (tablaLots.getSelectionModel().getSelectedItem() != null) {
			String id = tablaLots.getSelectionModel().getSelectedItem().getIdLot();
			int index = 999;
			for (int i = 0; i < producte.getLots().size(); i++) {
				if ((producte.getLots().get(i) != null && (producte.getLots().get(i).getLot() + "").equals(id + ""))) {
					index = i;
				}
			}
			producte.getLots().remove(index);
			tablaLots.getColumns().clear();
			listaLotVisual.clear();
			datoslots = FXCollections.observableArrayList(listaLotVisual);
			initialize(null, null);

		}
	}

	// tablaLots.getColumns().clear();
	// listaLotVisual.clear();
	// producte.getLots().add(new LotDesglossat(0, new Date(), 0));
	// datoslots = FXCollections.observableArrayList(listaLotVisual);
	// initialize(null, null);

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		LotVisual lotVisual;
		tablaLots.setEditable(true);

		if (producte != null) {
			deshabilitarCampos(false);
			idproducte.setText(producte.getCodiProducte() + "");
			idproducte.setDisable(true);
			nomProducte.setText(producte.getNomProducte() + "");
			descripcio.setText(producte.getDescripcio() + "");
			preuVenda.setText(producte.getPreuVenda() + "");
			stock.setText(producte.getStock() + "");
			stockMinim.setText(producte.getStockMinim() + "");
			idProveidor.setText(producte.getProveidor() != null ? producte.getProveidor().getIdProveidor() + "" : "");

			for (LotDesglossat lt : producte.getLots()) {

				String idlot = lt.getLot() + "";
				System.out.println(idlot);
				String gataEntr = lt.getDataEntrada() + "";
				String dataCad = lt.getDataCaducitat() + "";
				String quant = lt.getQuantitat() + "";

				lotVisual = new LotVisual(idlot, gataEntr, dataCad, quant);
				listaLotVisual.add(lotVisual);

			}

			switch (producte.getUnitat()) {
			case UNITAT:
				rduinitat.setSelected(true);
				break;
			case LLITRE:
				rdlitre.setSelected(true);
				break;
			case GRAMS:
				rdgrams.setSelected(true);
				break;
			default:
				break;
			}

			TableColumn<LotVisual, String> colidLot = new TableColumn<LotVisual, String>("idLot");
			colidLot.setCellValueFactory(new PropertyValueFactory<LotVisual, String>("idLot"));
			// colidLot.setCellFactory(new FieldEditable());

			colidLot.setCellFactory(TextFieldTableCell.<LotVisual>forTableColumn());
			colidLot.setOnEditCommit((CellEditEvent<LotVisual, String> t) -> {
				((LotVisual) t.getTableView().getItems().get(t.getTablePosition().getRow())).setIdLot(t.getNewValue());
			});

			TableColumn<LotVisual, String> colDataEntrada = new TableColumn<LotVisual, String>("Data Entrada");
			colDataEntrada.setCellValueFactory(new PropertyValueFactory<LotVisual, String>("dataEntrada"));
			// colDataEntrada.setCellFactory(new FieldEditable());

			colDataEntrada.setCellFactory(TextFieldTableCell.<LotVisual>forTableColumn());
			colDataEntrada.setOnEditCommit((CellEditEvent<LotVisual, String> t) -> {
				((LotVisual) t.getTableView().getItems().get(t.getTablePosition().getRow()))
						.setDataEntrada(t.getNewValue());
			});

			TableColumn<LotVisual, String> colDataCaducitat = new TableColumn<LotVisual, String>("Data Caducitat");
			colDataCaducitat.setCellValueFactory(new PropertyValueFactory<LotVisual, String>("dataCaducitat"));
			// colDataCaducitat.setCellFactory(new FieldEditable());

			colDataCaducitat.setCellFactory(TextFieldTableCell.<LotVisual>forTableColumn());
			colDataCaducitat.setOnEditCommit((CellEditEvent<LotVisual, String> t) -> {
				((LotVisual) t.getTableView().getItems().get(t.getTablePosition().getRow()))
						.setDataCaducitat(t.getNewValue());
			});

			TableColumn<LotVisual, String> colQuantitatLot = new TableColumn<LotVisual, String>("Quantitat");
			colQuantitatLot.setCellValueFactory(new PropertyValueFactory<LotVisual, String>("quantitat"));
			// colQuantitatLot.setCellFactory(new FieldEditable());

			colQuantitatLot.setCellFactory(TextFieldTableCell.<LotVisual>forTableColumn());
			colQuantitatLot.setOnEditCommit((CellEditEvent<LotVisual, String> t) -> {
				((LotVisual) t.getTableView().getItems().get(t.getTablePosition().getRow()))
						.setQuantitat(t.getNewValue());
			});

			datoslots = FXCollections.observableArrayList(listaLotVisual);
			tablaLots.setItems(datoslots);
			tablaLots.getColumns().addAll(colidLot, colDataEntrada, colDataCaducitat, colQuantitatLot);

		} else {
			deshabilitarCampos(true);

			// idproducte.focusedProperty().addListener(new ChangeListener<Boolean>()
			// {
			// @Override
			// public void changed(ObservableValue<? extends Boolean> arg0, Boolean
			// oldPropertyValue, Boolean newPropertyValue)
			// {
			// if (newPropertyValue)
			// {
			// deshabilitarCampos(true);
			//
			// }
			// else
			// {
			// deshabilitarCampos(false);
			// }
			// }
			//
			// });

			// deshabilitarCampos(true);
			//
			// idproducte.textProperty().addListener((observable, oldValue, newValue) -> {
			// System.out.println("textfield changed from " + oldValue + " to " + newValue);
			// deshabilitarCampos(false);
			// });

		}
	}

	public void setParentController(SeguimientoProducto seguimientoProducto) {
		segprod = seguimientoProducto;
	}

	public TextField getIdProducte() {
		return idproducte;
	}

	public void setIdProducte(TextField idProducte) {
		idproducte = idProducte;
	}

	public static Producte getProducte() {
		return producte;
	}

	public static void setProducte(Producte producte) {
		MaestroProducte.producte = producte;
	}

	public TextField getNomProducte() {
		return nomProducte;
	}

	public void setNomProducte(TextField nomProducte) {
		this.nomProducte = nomProducte;
	}

	public TextField getPreuVenda() {
		return preuVenda;
	}

	public void setPreuVenda(TextField preuVenda) {
		this.preuVenda = preuVenda;
	}

	public TextField getStock() {
		return stock;
	}

	public void setStock(TextField stock) {
		this.stock = stock;
	}

	public TextField getStockMinim() {
		return stockMinim;
	}

	public void setStockMinim(TextField stockMinim) {
		this.stockMinim = stockMinim;
	}

	public TextField getIdProveidor() {
		return idProveidor;
	}

	public void setIdProveidor(TextField idProveidor) {
		this.idProveidor = idProveidor;
	}

}
