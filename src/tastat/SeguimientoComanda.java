package tastat;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Entidades.Comanda;
import EntidadesParaVisual.ComandaVisual;
import EntidadesParaVisual.ProducteVisual;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.event.*;
import javafx.scene.control.TextField;

@SuppressWarnings("restriction")
public class SeguimientoComanda extends Application implements Initializable {

	@FXML
	private TableView<ComandaVisual> tablaComanda;
	@FXML
	private TextField filtroGeneral;
	@FXML
	private TextField filtroClient;
	@FXML
	private TextField filtroEstat;
	@FXML
	private Button BTNovaComanda;
	@FXML
	private Button BTEliminarComanda;
	@FXML
	private Button BTModificar;

	public static int retornoMaestro;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm zzz");

	@Override
	public void start(Stage primaryStage) throws Exception {

	}

	public void lanzarNuevaComanda(ActionEvent event) {
		MaestroComanda.setComanda(null);
		Stage parentStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
		Stage primaryStage = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("tastat/MaestroComanda.fxml"));
		Parent root = null;
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}

		MaestroComanda controller = (MaestroComanda) loader.getController();
		controller.setParentController(this);

		Scene scene = new Scene(root);

		primaryStage.setTitle("Nova Comanda");
		primaryStage.setScene(scene);
		primaryStage.initModality(Modality.APPLICATION_MODAL);
		primaryStage.initOwner(parentStage);
		primaryStage.showAndWait();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		TableColumn<ComandaVisual, String> colIdComanda = new TableColumn<ComandaVisual, String>("ID");
		colIdComanda.setCellValueFactory(new PropertyValueFactory<ComandaVisual, String>("idComanda"));

		TableColumn<ComandaVisual, String> colIdClient = new TableColumn<ComandaVisual, String>("Id Client");
		colIdClient.setCellValueFactory(new PropertyValueFactory<ComandaVisual, String>("idClient"));

		TableColumn<ComandaVisual, String> colNomClient = new TableColumn<ComandaVisual, String>("Nom Client");
		colNomClient.setCellValueFactory(new PropertyValueFactory<ComandaVisual, String>("nomClient"));

		TableColumn<ComandaVisual, String> colDataComanda = new TableColumn<ComandaVisual, String>("Data Comanda");
		colDataComanda.setCellValueFactory(new PropertyValueFactory<ComandaVisual, String>("dataComanda"));

		TableColumn<ComandaVisual, String> colDataLliurament = new TableColumn<ComandaVisual, String>(
				"Data Lliurament");
		colDataLliurament.setCellValueFactory(new PropertyValueFactory<ComandaVisual, String>("dataLliurament"));

		TableColumn<ComandaVisual, String> colEstat = new TableColumn<ComandaVisual, String>("Estat");
		colEstat.setCellValueFactory(new PropertyValueFactory<ComandaVisual, String>("estat"));

		ComandaVisual comandaVisual;
		ArrayList<ComandaVisual> listaComandaVisual = new ArrayList<>();

		ObservableList<ComandaVisual> datos = null;

		for (Comanda c : Main.getElMeuMagatzem().getComandes()) {
			String idComanda = c.getIdComanda() + "";
			String idClient = c.getClient().getIdClient() + "";
			String nomClient = c.getClient().getNomClient() + "";
			String dataComanda = sdf.format(c.getDataComanda()) + "";
			String dataLliurament = sdf.format(c.getDataLliurament()) + "";
			String estat = c.getEstat() + "";

			comandaVisual = new ComandaVisual(idComanda, idClient, nomClient, dataComanda, dataLliurament, estat);

			listaComandaVisual.add(comandaVisual);
		}

		datos = FXCollections.observableArrayList(listaComandaVisual);
		tablaComanda.setItems(datos);

		tablaComanda.getColumns().addAll(colIdComanda, colIdClient, colNomClient, colDataComanda, colDataLliurament,
				colEstat);

		FilteredList<ComandaVisual> filteredData = new FilteredList<>(datos, p -> true);

		// 2. Set the filter Predicate whenever the filter changes.
		filtroGeneral.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(com -> {
				// If filter text is empty, display all.
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();

				if (com.getIdComanda().toLowerCase().contains(lowerCaseFilter)) {
					return true;
				} else if (com.getIdClient().toLowerCase().contains(lowerCaseFilter)) {
					return true;
				} else if (com.getNomClient().toLowerCase().contains(lowerCaseFilter)) {
					return true;
				} else if (com.getDataComanda().toLowerCase().contains(lowerCaseFilter)) {
					return true;
				} else if (com.getDataLliurament().toLowerCase().contains(lowerCaseFilter)) {
					return true;
				} else if (com.getEstat().toLowerCase().contains(lowerCaseFilter)) {
					return true;
				}
				return false; // Does not match.--
			});
		});

		// 3. Wrap the FilteredList in a SortedList.
		SortedList<ComandaVisual> sortedData = new SortedList<>(filteredData);

		// 4. Bind the SortedList comparator to the TableView comparator.
		sortedData.comparatorProperty().bind(tablaComanda.comparatorProperty());

		// 5. Add sorted (and filtered) data to the table.
		tablaComanda.setItems(sortedData);

		// ******************
		FilteredList<ComandaVisual> filteredData2;

		FilteredList<ComandaVisual> fl = new FilteredList<ComandaVisual>(sortedData);

		filteredData2 = filtrarClient(fl);
		SortedList<ComandaVisual> sortedData2 = new SortedList<>(filteredData2);
		sortedData2.comparatorProperty().bind(tablaComanda.comparatorProperty());
		tablaComanda.setItems(sortedData2);

		// ******************
		FilteredList<ComandaVisual> filteredData3;

		FilteredList<ComandaVisual> fl1 = new FilteredList<ComandaVisual>(sortedData2);

		filteredData3 = filtrarEstat(fl1);
		SortedList<ComandaVisual> sortedData3 = new SortedList<>(filteredData3);
		sortedData3.comparatorProperty().bind(tablaComanda.comparatorProperty());
		tablaComanda.setItems(sortedData3);
	}

	@FXML	
	public FilteredList<ComandaVisual> filtrarClient(FilteredList<ComandaVisual> filteredData) {
		 // 2. Set the filter Predicate whenever the filter changes.
		filtroClient.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(person -> {
                // If filter text is empty, display all.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (person.getNomClient().toLowerCase().contains(lowerCaseFilter)) { 
                    return true; 
                }
                if (person.getIdClient().toLowerCase().contains(lowerCaseFilter)) { 
                    return true; 
                }
                return false; // Does not match.--
            });
        });
		
		return filteredData;
	}
	
	@FXML	
	public FilteredList<ComandaVisual> filtrarEstat(FilteredList<ComandaVisual> filteredData) {
		 // 2. Set the filter Predicate whenever the filter changes.
		filtroEstat.textProperty().addListener((observable, oldValue, newValue) -> {
           filteredData.setPredicate(person -> {
               // If filter text is empty, display all.
               if (newValue == null || newValue.isEmpty()) {
                   return true;
               }
               
              //Compare
               String lowerCaseFilter = newValue.toLowerCase();
               
               if (person.getEstat().toLowerCase().contains(lowerCaseFilter)) { 
                   return true; 
               }
               return false; // Does not match.--
           });
       });
		
		return filteredData;
		
	}
	
	public void gestionarAvisoAlModificar() {
		Alert aviso = new Alert(AlertType.WARNING);
		aviso.setTitle("Modificaciķ");
		aviso.setHeaderText(null);
		aviso.setContentText("Si us plau, seleccioni un comanda a modificar");
		aviso.showAndWait();
	}

	public void gestionarAvisoAlEliminar() {
		Alert aviso = new Alert(AlertType.WARNING);
		aviso.setTitle("Eliminaciķ");
		aviso.setHeaderText(null);
		aviso.setContentText("Si us plau, seleccioni un comanda a eliminar");
		aviso.showAndWait();
	}

	public void lanzarModificarComanda(ActionEvent event) {
		if (tablaComanda.getSelectionModel().getSelectedItem() == null) {
			gestionarAvisoAlModificar();

		} else {
			MaestroComanda.setComanda(null);
			String id = tablaComanda.getSelectionModel().getSelectedItem().getIdComanda();

			for (Comanda c : Main.getElMeuMagatzem().getComandes()) {
				if ((c.getIdComanda() + "").equals(id)) {
					MaestroComanda.setComanda(c);
				}
			}

			Stage parentStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("tastat/MaestroComanda.fxml"));
			Parent root = null;
			try {
				root = loader.load();
			} catch (IOException e) {
				e.printStackTrace();
			}

			MaestroComanda controller = (MaestroComanda) loader.getController();
			controller.setParentController(this);

			Scene scene = new Scene(root);

			primaryStage.setTitle("Catāleg de la comanda " + MaestroComanda.getComanda().getIdComanda());
			primaryStage.setScene(scene);
			primaryStage.initModality(Modality.APPLICATION_MODAL);
			primaryStage.initOwner(parentStage);
			primaryStage.showAndWait();
		}
	}

	@FXML
	public void eliminarComanda() {
		if (tablaComanda.getSelectionModel().getSelectedItem() != null) {
			String id = tablaComanda.getSelectionModel().getSelectedItem().getIdComanda();
			int index = 999;
			for (int i = 0; i < Main.getElMeuMagatzem().getComandes().size(); i++) {
				if ((Main.getElMeuMagatzem().getComandes().get(i) != null
						&& (Main.getElMeuMagatzem().getComandes().get(i).getIdComanda() + "").equals(id + ""))) {
					index = i;
				}
			}
			Main.getElMeuMagatzem().getComandes().remove(index);

			refrescar();
		} else {
			gestionarAvisoAlEliminar();
		}
	}

	public void refrescar() {
		tablaComanda.getColumns().clear();
		initialize(null, null);
	}
}
