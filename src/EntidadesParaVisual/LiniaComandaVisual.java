package EntidadesParaVisual;

import javafx.beans.property.SimpleStringProperty;

@SuppressWarnings("restriction")
public class LiniaComandaVisual {

	private SimpleStringProperty nLinia;
	private SimpleStringProperty idProducte;
	private SimpleStringProperty nomProducte;
	private SimpleStringProperty quantitat;
	private SimpleStringProperty preu;
	private SimpleStringProperty importe;

	public LiniaComandaVisual(String idProducte, String nomProducte, String quantitat, String preu) {

		this.idProducte = new SimpleStringProperty(idProducte);
		this.nomProducte = new SimpleStringProperty(nomProducte);
		this.quantitat = new SimpleStringProperty(quantitat);
		this.preu = new SimpleStringProperty(preu);
		this.importe = new SimpleStringProperty((Integer.parseInt(quantitat) * Double.parseDouble(preu)) + "");
	}

	public LiniaComandaVisual() {
	}

	public String getnLinia() {
		return nLinia.get();
	}

	public void setnLinia(String nLinia) {
		this.nLinia.set(nLinia);
		;
	}

	public String getIdProducte() {
		return idProducte.get();
	}

	public void setIdProducte(String idProducte) {
		this.idProducte.set(idProducte);
		;
	}

	public String getNomProducte() {
		return nomProducte.get();
	}

	public void setNomProducte(String nomProducte) {
		this.nomProducte.set(nomProducte);
	}

	public String getQuantitat() {
		return quantitat.get();
	}

	public void setQuantitat(String quantitat) {
		this.quantitat.set(quantitat);
		;
	}

	public String getPreu() {
		return preu.get();
	}

	public void setPreu(String preu) {
		this.preu.set(preu);
		;
	}

	public String getImporte() {
		return importe.get();
	}

	public void setImporte(String importe) {
		this.importe.set(importe);
	}

}
