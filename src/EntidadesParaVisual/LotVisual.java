package EntidadesParaVisual;


import javafx.beans.property.SimpleStringProperty;


public class LotVisual {
	
	private SimpleStringProperty idLot;
	private SimpleStringProperty dataEntrada;
	private SimpleStringProperty dataCaducitat;
	private SimpleStringProperty quantitat;
	
	public LotVisual(String idLot,String dataEntrada,String dataCaducitat,String quantitat){
		this.idLot = new SimpleStringProperty(idLot);
		this.dataEntrada = new SimpleStringProperty(dataEntrada);
		this.dataCaducitat = new SimpleStringProperty(dataCaducitat); 
		this.quantitat = new SimpleStringProperty(quantitat);
 
		
		
	}
	
	/*GETTERS*/
	public String getIdLot() {
		return idLot.get();
	}
	
	public String getDataEntrada() {
		return dataEntrada.get();
	}
	
	public String getDataCaducitat() {
		return dataCaducitat.get();
	}
	public String getQuantitat() {
		return quantitat.get();
	}
	
	
	/*SETTERS*/

	public void setIdLot(String idLot) {
		this.idLot.set(idLot);
	}
	
	public void setDataEntrada(String dataEntrada) {
		this.dataEntrada.set(dataEntrada);
	}
	
	public void setDataCaducitat(String dataCaducitat) {
		this.dataCaducitat.set(dataCaducitat);
	}
	
	public void setQuantitat(String quantitat) {
		this.quantitat.set(quantitat);
	}
	
}