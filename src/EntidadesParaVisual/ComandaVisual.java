package EntidadesParaVisual;

import javafx.beans.property.SimpleStringProperty;

@SuppressWarnings("restriction")
public class ComandaVisual {

	private SimpleStringProperty idComanda;
	private SimpleStringProperty idClient;
	private SimpleStringProperty nomClient;
	private SimpleStringProperty dataComanda;
	private SimpleStringProperty dataLliurament;
	private SimpleStringProperty estat;

	public ComandaVisual(String idComanda, String idClient, String nomClient, String dataComanda, String dataLliurament,
			String estat) {

		this.idComanda = new SimpleStringProperty(idComanda);
		this.idClient = new SimpleStringProperty(idClient);
		this.nomClient = new SimpleStringProperty(nomClient);
		this.dataComanda = new SimpleStringProperty(dataComanda);
		this.dataLliurament = new SimpleStringProperty(dataLliurament);
		this.estat = new SimpleStringProperty(estat);
	}

	public String getIdComanda() {
		return idComanda.get();
	}

	public void setIdComanda(String idComanda) {
		this.estat.set(idComanda);
		;
	}

	public String getIdClient() {
		return idClient.get();
	}

	public void setIdClient(String idClient) {
		this.estat.set(idClient);
		;
	}

	public String getNomClient() {
		return nomClient.get();
	}

	public void setNomClient(String nomClient) {
		this.estat.set(nomClient);
		;
	}

	public String getDataComanda() {
		return dataComanda.get();
	}

	public void setDataComanda(String dataComanda) {
		this.estat.set(dataComanda);
		;
	}

	public String getDataLliurament() {
		return dataLliurament.get();
	}

	public void setDataLliurament(String dataLliurament) {
		this.estat.set(dataLliurament);
		;
	}

	public String getEstat() {
		return estat.get();
	}

	public void setEstat(String estat) {
		this.estat.set(estat);
		;
	}

}
