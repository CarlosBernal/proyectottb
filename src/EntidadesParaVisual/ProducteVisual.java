package EntidadesParaVisual;


import javafx.beans.property.SimpleStringProperty;


public class ProducteVisual {
	
	private SimpleStringProperty codiProducte;
	private SimpleStringProperty nomProducte;
	private SimpleStringProperty quantitat;
	private SimpleStringProperty tipoProducte;
	private SimpleStringProperty descripcio;
	private SimpleStringProperty unitat;
	
	public ProducteVisual(String codiProducte,String nomProducte,String quantitat,String tipoProducte,String unitat,String descripcio){
		this.codiProducte = new SimpleStringProperty(codiProducte);
		this.nomProducte = new SimpleStringProperty(nomProducte);
		this.quantitat = new SimpleStringProperty(quantitat); 
		this.tipoProducte = new SimpleStringProperty(tipoProducte);
		this.descripcio = new SimpleStringProperty(descripcio);
		this.unitat = new SimpleStringProperty(unitat);
	}
	
	
	/*GETTERS*/
	
	public String getDescripcio() {
		return descripcio.get();
	}

	public String getCodiProducte() {
		return codiProducte.get();
	}
	
	public String getNomProducte() {
		return nomProducte.get();
	}
	
	public String getQuantitat() {
		return quantitat.get();
	}
	public String getTipoProducte() {
		return tipoProducte.get();
	}
	
	public String getUnitat() {
		return unitat.get();
	}
	 
	/*SETTERS*/

	public void setCodiProducte(String codiProducte) {
		this.codiProducte.set(codiProducte);
	}
	
	public void setNomProducte(String nomProducte) {
		this.nomProducte.set(nomProducte);
	}
	
	public void setQuantitat(String quantitat) {
		this.quantitat.set(quantitat);
	}
	
	public void setTipoProducte(String tipoProducte) {
		this.tipoProducte.set(tipoProducte);
	}
	public void setDescripcio(String descripcio) {
		this.descripcio.set(descripcio);
	}

	public void setUnitat(String unitat) {
		this.unitat.set(unitat);
	}
	
	
}