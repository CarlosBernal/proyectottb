package Entidades;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import EntidadesParaVisual.LotVisual;
import tastat.Generador;
import tastat.Tools;

public class Producte implements Comparable<Producte> {

	private int codiProducte;
	private String nomProducte;
	private String descripcio;
	private int stockMinim;
	private UnitatMesura unitat;
	private Map<Producte, Integer> composicio;
	private ArrayList<LotDesglossat> lots;
	private Tipus tipus;
	private Proveidor proveidor;
	private double preuVenda;
	private double pes;

	public void afegirLot(int quantitat, Date dataCaducitat) {
		int qLot = Generador.getNextLot();
		this.lots.add(new LotDesglossat(qLot, dataCaducitat, quantitat));
	}

	public String veureLots() {
		String cadena = "";
		for (LotDesglossat ld : lots) {
			cadena += "   " + ld + "\n";
		}
		return cadena;
	}

	public String getNomProducte() {
		return nomProducte;
	}

	public void setNomProducte(String nom) {
		nomProducte = nom;
	}

	@Override
	public String toString() {
		String cadena = "Producte: " + codiProducte + "\t - " + nomProducte + "\tStock Total: " + getStock() + " "
				+ unitat;
		cadena = cadena + "\tStockMínim:" + stockMinim + "\t" + tipus;
		return cadena;
	}

	Producte() {
		codiProducte = Generador.getNextProducte();
		lots = new ArrayList<LotDesglossat>();
		composicio = new HashMap<Producte, Integer>();
		tipus = Tipus.INGREDIENT;
		stockMinim = 0;

	}

	public Producte(String nomProducte) {
		this();
		this.nomProducte = nomProducte;
	}

	public Producte(String nomProducte, UnitatMesura u, int sm) {
		this(nomProducte);
		this.setUnitatMesura(u);
		this.stockMinim = sm;
	};

	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}

	public Tipus getTipus() {
		return tipus;
	}

	public void setProveidor(Proveidor pv) {
		this.proveidor = pv;
	}

	public Proveidor getProveidor() {
		return proveidor;
	}

	public UnitatMesura getUnitatMesura() {
		return unitat;
	}

	public void setUnitatMesura(UnitatMesura unitatm) {
		unitat = unitatm;
	}

	// FEFO
	public void setStock(int q) {
		if (q <= this.getStock()) {
			while (q > 0) {
				if (q < this.getLotFEFO().getQuantitat()) {
					this.getLotFEFO().setQuantitat(q - (this.getLotFEFO().getQuantitat() - this.getStock()));
					q = 0;
				} else {
					q = q - this.getLotFEFO().getQuantitat();
					this.getLots().remove(this.getLotFEFO());
				}
			}
		} else {
			Date dt = new Date();
			dt = Tools.sumarDies(dt, 10);

			this.getLots().add(new LotDesglossat(this.getLots().size(), dt, q - this.getStock()));
		}
	}

	public LotDesglossat getLotFEFO() {
		if (this.getLots() != null && this.getLots().size() > 0) {
			LotDesglossat fefo = this.getLots().get(0);
			for (LotDesglossat l : this.getLots()) {
				if (l.compareTo(fefo) <= 0) {
					fefo = l;
				}
			}

			return fefo;
		} else {
			System.out.println("No queden lots d'aquest producte");
			return null;
		}

	}

	public void setStockMinim(int stockM) {
		stockMinim = stockM;
	}

	public int getStockMinim() {
		return stockMinim;
	}

	public void setPreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}

	public double getPreuVenda() {
		return preuVenda;
	}

	public Map<Producte, Integer> getComposicio() {
		return composicio;
	}

	public void afegirComponent(Producte p, int q) {
		composicio.put(p, q);
	}

	public String veureComposicio() {
		String cadena = "";
		Set<Producte> claus = composicio.keySet();
		cadena = getNomProducte() + " --> ";
		for (Producte p : claus)
			cadena += p.getNomProducte() + "(" + composicio.get(p) + ") ";
		return cadena;
	}

	@Override
	public int compareTo(Producte p) {
		return (getNomProducte().compareTo(p.getNomProducte()));
	}

	public int getCodiProducte() {
		return codiProducte;
	}

	private int calcularStockLote() {
		int q = 0;
		for (LotDesglossat l : lots) {
			q += l.getQuantitat();
		}
		return q;
	}

	public int getStock() {
		return this.calcularStockLote();
	}

	public String veureLotsOrdenats() {
		lots.sort(null);
		String cadena = "";
		for (LotDesglossat ld : lots) {
			cadena += "   " + ld + "\n";
		}
		return cadena;
	}

	public UnitatMesura getUnitat() {
		return unitat;
	}

	public void setUnitat(UnitatMesura unitat) {
		this.unitat = unitat;
	}

	public ArrayList<LotDesglossat> getLots() {
		return lots;
	}

	public void setLots(ArrayList<LotDesglossat> lots) {
		this.lots = lots;
	}

	public double getPes() {
		return pes;
	}

	public void setPes(double pes) {
		this.pes = pes;
	}

	public void setCodiProducte(int codiProducte) {
		this.codiProducte = codiProducte;
	}

	public void setComposicio(Map<Producte, Integer> composicio) {
		this.composicio = composicio;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

}