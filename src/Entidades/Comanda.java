package Entidades;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tastat.Generador;
import tastat.Tools;

public class Comanda {
	private int idComanda;
	private Client client;
	private Date dataComanda;
	private Date dataLliurament;
	private ComandaEstat estat; // PENDENT - PREPARAT - TRANSPORT - LLIURAT
	private Double portes; // preu de transport
	private List<ComandaLinia> linies;

	public Comanda() {
		idComanda = Generador.getNextComanda();
		dataComanda = new Date();
		dataLliurament = Tools.sumarDies(new Date(), 1);
		estat = ComandaEstat.PENDENT;
		portes = 0.0;
		linies = new ArrayList<ComandaLinia>();
	}

	public Comanda(Client client) {
		this();
		this.client = client;
	};

	public List<ComandaLinia> getLinies() {
		return linies;
	}

	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public LocalDate getDataComandaLocal() {
		return Instant.ofEpochMilli(dataComanda.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	
	public Date getDataComanda() {
		return dataComanda;
	}

	public Date getDataLliurament() {
		return dataLliurament;
	}

	public void setDataComanda(Date dataComanda) {
		this.dataComanda = dataComanda;
	}

	public LocalDate getDataLliuramentLocal() {
		return Instant.ofEpochMilli(dataLliurament.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public void setDataLliurament(Date dataLliurament) {
		this.dataLliurament = dataLliurament;
	}

	public ComandaEstat getEstat() {
		return estat;
	}

	public void setEstat(ComandaEstat estat) {
		this.estat = estat;
	}

	public Double getPortes() {
		return portes;
	}

	public void setPortes(Double portes) {
		this.portes = portes;
	}

	public void setLinies(List<ComandaLinia> linies) {
		this.linies = linies;
	}

	
	
}